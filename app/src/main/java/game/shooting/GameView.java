package game.shooting;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.example.leeah.app_0714_graphicmedia.R;

public class GameView extends View {
    Bitmap ship;
    private int shipX;
    private int shipY = 300;
    Paint paint;
    // 조이스틱, 총알버튼의 크기
    RectF[] control   = new RectF[5];
    int controlWidth  = 100;
    int controlHeight = 100;
    int standardTop   = 700;
    int standardLeft  = 210;



    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ship);
        ship = Bitmap.createScaledBitmap(bitmap, 200, 150, false);

    /*
        Matrix matrix = new Matrix();       // Matrix : 이미지 왜곡시
        matrix.setScale(100, 100);
    */

        paint = new Paint();
        paint.setColor(Color.BLUE);

        control[0] = new RectF(standardLeft, standardTop,
                                standardLeft+controlWidth, standardTop+controlHeight);

        control[1] = new RectF(standardLeft-110, standardTop+110,
                                (standardLeft-110)+controlWidth, (standardTop+110)+controlHeight);

        control[2] = new RectF(standardLeft, (standardTop+110),
                                standardLeft+controlWidth, (standardTop+110)+controlHeight);

        control[3] = new RectF(standardLeft+110, (standardTop+110),
                                (standardLeft+100)+controlWidth, (standardTop+110)+controlHeight);

        control[4] = new RectF(standardLeft+1500, (standardTop+110),
                                (standardLeft+1500)+controlWidth, (standardTop+110)+controlHeight);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(ship, shipX, shipY, null);

        paintJoyStick(canvas);
    }


    /*
        # 조이스틱 그리기
     */
    public void paintJoyStick(Canvas canvas){
        for( int i = 0; i < control.length; i++ ){
            canvas.drawRect(control[i], paint);
        }
    }


    public int getShipX() {
        return shipX;
    }

    public int getShipY() {
        return shipY;
    }

    public void setShipX(int shipX) {
        this.shipX = shipX;
    }

    public void setShipY(int shipY) {
        this.shipY = shipY;
    }
}
