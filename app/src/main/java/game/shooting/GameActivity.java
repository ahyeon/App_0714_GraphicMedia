package game.shooting;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;

import com.example.leeah.app_0714_graphicmedia.R;

public class GameActivity extends Activity{
    GameView gameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        gameView = (GameView) findViewById(R.id.gameView);
    }

    /*
        우주선 날리자!  Bitmap의 x 축 변경 후 invalidate
     */
    public void moveShip(){
        int shipY = gameView.getShipX();
        gameView.setShipX(shipY+50);
        gameView.invalidate();
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        moveShip();
        return false;
    }
}
