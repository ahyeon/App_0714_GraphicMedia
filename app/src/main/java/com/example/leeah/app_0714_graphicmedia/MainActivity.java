package com.example.leeah.app_0714_graphicmedia;

import android.app.Activity;
import android.graphics.Point;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;

import java.lang.reflect.Array;
import java.util.ArrayList;


public class MainActivity extends Activity {
    String              TAG;
    MyView              myView;
    ArrayList<Point>    pointArray = new ArrayList<Point>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TAG    = this.getClass().getName();
        myView = (MyView) findViewById(R.id.myView);
    }


    public void printOval(ArrayList<Point> list){
        // 정해진 좌표 이용하여 MyView 다시 그리기
        myView.list = list;
        myView.invalidate();
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d("-----[x]", Float.toString(event.getX()));
        Log.d("-----[y]", Float.toString(event.getY()));

        //printOval((int) event.getX(), (int) event.getY());

        /* 터치 발생시 컬렉션 객체에 담자 */
        Point point = new Point((int) event.getX(), (int) event.getY());
        pointArray.add(point);
        printOval(pointArray);

        return false;
    }
}

